#Stage 1 - Install dependencies and build the app
FROM sankalpamokal/flutter-web:latest AS build-env

# ENV http_proxy 'http://'
# ENV https_proxy 'http://'
# ENV HTTP_PROXY "http://"
# ENV HTTPS_PROXY "http://"


# Copy files to container and build
RUN mkdir /usr/local/myproj
COPY . /usr/local/myproj
WORKDIR /usr/local/myproj/
RUN /usr/local/flutter/bin/flutter channel beta
# RUN /usr/local/flutter/bin/flutter upgrade
RUN /usr/local/flutter/bin/flutter build web

# Stage 2 - Create the run-time image
FROM nginx
COPY --from=build-env /usr/local/myproj/build/web /usr/share/nginx/html

EXPOSE 80
